### Infomation ###
* Quick summary
Auto PVP
* Version
1.0
### Commit guidelines ###
* [Feature]: Commits which are features should start with [Feature] and followed by a quick summary on the top line, followed by some extra details in the commit body.
* [Fix]: Commits which fix bugs, or minor improvements to existing features should start with [Fix] and followed by a quick summary on the top line, followed by some extra details in the commit body.
* Commits that relate to the last commit don't need a prefix
* All Commits that are still a work in progress should go to the branch **Working**
* Commits that are finished and that work can go to the branch **Master**
### Contact ###

* JustanJap | Skype: justanjap