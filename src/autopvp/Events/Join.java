package autopvp.Events;

import autopvp.Main.Main;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class Join implements Listener {
	@EventHandler
	public void onJoin(final PlayerJoinEvent e) {
		Player p = e.getPlayer();
		FileConfiguration config = Main.getPlugin().getConfig();
		if (!(config.contains("players." + p.getUniqueId() + ".toggles.pvp-play"))) {
			String place = "players." + p.getUniqueId() + ".toggles.pvp-play";
			config.set(place, true);
			Main.getPlugin().saveConfig();
		}
		if (!config.contains("players." + p.getUniqueId())) {
			// first time join
			config.set("players." + p.getUniqueId() + ".name", p.getName());
			config.set("players." + p.getUniqueId() + ".ip", p.getAddress());
			Main.getPlugin().saveConfig();
		} else { // changes check
			if (!config.get("players." + p.getUniqueId() + ".ip").equals(
					p.getAddress())) {
				config.set("players." + p.getUniqueId() + ".ip", p.getAddress());
			}
			if (!config.get("players." + p.getUniqueId() + ".name").equals(
					p.getName())) {
				config.set("players." + p.getUniqueId() + ".name", p.getName());
			}
			Main.getPlugin().saveConfig();
		}
	}
}