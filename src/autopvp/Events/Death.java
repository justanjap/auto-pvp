package autopvp.Events;

import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;




import autopvp.Main.Util;

public class Death implements Listener {
	
	@EventHandler
	public void onNoHealth(final EntityDamageEvent e) {
		Entity entity = e.getEntity();
		if (entity.getType().equals(EntityType.PLAYER)) {
			Player p = (Player) entity;
			double health = p.getHealth() - p.getLastDamage();
			if (health <= 0) {
				if (Util.inGame.get(p) == true) {
					p.setHealth(20.0);
					Util.gameOver(p);
				}
			}
		}
	}
}
