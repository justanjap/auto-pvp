package autopvp.Events;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import autopvp.Main.Util;

public class Leave implements Listener {
	@EventHandler
	public void onLeave(final PlayerQuitEvent e) {
		Player p = e.getPlayer();
		if (Util.inGame.get(p) == true){
			Util.gameOver(p);
		}
	}
}
