package autopvp.Main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;
import net.minecraft.server.v1_8_R3.IChatBaseComponent.ChatSerializer;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitScheduler;

public class Util {
	public static FileConfiguration config = Main.getPlugin().getConfig();

	public static HashMap<Player, Boolean> inGame = new HashMap<Player, Boolean>();
	public static HashMap<String, Boolean> gameOn = new HashMap<String, Boolean>();

	public static Boolean toggle(Player p, String type) {
		String place = "players." + p.getUniqueId() + ".toggles." + type;
		Boolean state = null;

		if (!config.contains(place) || config.getBoolean(place) == false) {
			config.set(place, true);
			Main.getPlugin().saveConfig();
			state = true;
		} else {
			config.set(place, false);
			Main.getPlugin().saveConfig();
			state = false;
		}
		return state;
	}

	public static void gameStart() {
		int dist = 100000;
		int pDist = 32;
		int centerX = new Random().nextInt(dist * 2) - dist;
		int centerZ = new Random().nextInt(dist * 2) - dist;
		int intPlayers = 0;
		for (final Player p : Bukkit.getOnlinePlayers()) {
			if (config.getBoolean("players." + p.getUniqueId()
					+ ".toggles.pvp-play") == true) {
				intPlayers++;
				inGame.put(p, true);
				Location oldLocation = p.getLocation();
				config.set("players." + p.getUniqueId() + ".old-location",
						oldLocation);
				config.set("players." + p.getUniqueId() + ".old-inv.inv", p
						.getInventory().getContents());
				config.set("players." + p.getUniqueId() + ".old-inv.armor", p
						.getInventory().getArmorContents());
				Main.getPlugin().saveConfig();
				int playerX = new Random().nextInt(pDist * 2) - pDist;
				int playerZ = new Random().nextInt(pDist * 2) - pDist;
				int x = centerX + playerX;
				int z = centerZ + playerZ;
				int y = 256;
				String world = config.getString("server-info.tpworld");
				Location teleportLocation = new Location(
						Bukkit.getWorld(world), x, y, z);
				p.addPotionEffect(new PotionEffect(PotionEffectType.HEAL, 15,
						10, true, false));
				p.addPotionEffect(new PotionEffect(
						PotionEffectType.DAMAGE_RESISTANCE, 15, 10, true, false));
				p.addPotionEffect(new PotionEffect(
						PotionEffectType.WATER_BREATHING, 15, 10, true, false));
				p.addPotionEffect(new PotionEffect(
						PotionEffectType.FIRE_RESISTANCE, 15, 10, true, false));
				p.addPotionEffect(new PotionEffect(PotionEffectType.WEAKNESS,
						15, 10, true, false));
				boolean isOnLand = false;
				while (isOnLand == false) {
					teleportLocation = new Location(p.getWorld(), x, y, z);
					if (teleportLocation.getBlock().getType() != Material.AIR) {
						isOnLand = true;
					} else
						y--;
				}
				y += 25;
				p.teleport(new Location(p.getWorld(), teleportLocation.getX(),
						teleportLocation.getY() + 1, teleportLocation.getZ()));
				sendActionBar(p, ChatColor.GREEN + "PVP starts in 30 seconds");
				int secs = 20;
				int start = 15 * secs;
				BukkitScheduler scheduler = Bukkit.getServer().getScheduler();
				scheduler.scheduleSyncDelayedTask(Main.getPlugin(),
						new Runnable() {
							@Override
							public void run() {
								p.sendMessage(ChatColor.GREEN
										+ "Fight has started!");
								gameOn.put("game", true);
							}
						}, start);
			}
		}
		if (intPlayers <= 0) {
			gameOn.put("game", false);
			Player loner = (Player) inGame.keySet();
			loner.sendMessage(ChatColor.RED
					+ "No players to play with. Sorry you loner.");
			gameOver(loner);
		}
	}

	@SuppressWarnings("unchecked")
	public static void gameOver(Player p) {
		p.teleport((Location) config.get("players." + p.getUniqueId()
				+ ".old-location"));
		ArrayList<String> content = (ArrayList<String>) config.get("players."
				+ p.getUniqueId() + ".old-inv.inv");
		ArrayList<String> armor = (ArrayList<String>) config.get("players."
				+ p.getUniqueId() + ".old-inv.armor");
		p.getInventory().clear();
		p.getInventory().setContents(
				content.toArray(new ItemStack[content.size()]));
		p.getInventory().setArmorContents(
				armor.toArray(new ItemStack[armor.size()]));
		inGame.remove(p);
		if (gameOn.get("game") == true) {
			int peopleIn = inGame.size();
			for (final Player players : Bukkit.getOnlinePlayers()) {
				if (config.getBoolean("players." + p.getUniqueId()
						+ ".toggles.pvp-play") == true) {
					if (peopleIn != 1) {
						players.sendMessage(ChatColor.RED + "Only "
								+ ChatColor.GREEN + peopleIn + ChatColor.RED
								+ " people remaining in the fight");
					} else {
						Player winner = (Player) inGame.keySet();
						players.sendMessage(ChatColor.GREEN + ""
								+ ChatColor.BOLD + winner.getCustomName()
								+ "won the Fight!");
						players.sendMessage(ChatColor.GREEN + ""
								+ ChatColor.BOLD
								+ "He won $250,000 and 32 Diamonds!");
						Bukkit.dispatchCommand(Bukkit.getConsoleSender(),
								"eco give " + winner.getName() + " 250000");
						ItemStack item = new ItemStack(Material.DIAMOND, 32);
						if (p.getInventory().addItem(item).size() != 0) {
							p.getWorld().dropItemNaturally(p.getLocation(),
									item);
						}
					}
				}
			}
		}
	}

	public static void sendActionBar(Player player, String message) {
		Player p = (Player) player;
		IChatBaseComponent cbc = ChatSerializer.a("{\"text\": \"" + message
				+ "\"}");
		PacketPlayOutChat ppoc = new PacketPlayOutChat(cbc, (byte) 2);
		((CraftPlayer) p).getHandle().playerConnection.sendPacket(ppoc);
	}

}
