//comments are for examples or for guidance
package autopvp.Main;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.event.Listener;
import org.bukkit.permissions.Permission;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitScheduler;

import autopvp.Events.Death;
import autopvp.Events.Join;
import autopvp.Events.Leave;
import autopvp.commands.AutoPvp;

public class Main extends JavaPlugin implements Listener {
	private static Plugin plugin;

	@Override
	public void onEnable() {
		plugin = this;
		registerEvents();
		//registerCommands();
		registerPerms();
		createConfig();
		System.out.println("AP: Plugin Enabled");
		Util.gameOn.put("game", false);
		int secs = 20;
		int minutes = 60 * secs;
		BukkitScheduler scheduler = Bukkit.getServer().getScheduler();
		scheduler.scheduleSyncRepeatingTask(this, new Runnable() {
			@Override
			public void run() {
				if (Util.gameOn.get("game") == false) {
					Util.gameStart();
				}
			}
		}, 5 * minutes, 30 * minutes);
	}

	@Override
	public void onDisable() {
		plugin = null;
	}

	/*public void registerCommands() {
		// add commands here
		getCommand("ap").setExecutor(new AutoPvp());
		getCommand("autopvp").setExecutor(new AutoPvp());
	}*/

	public void registerEvents() {
		// add events here
		eventRegister(this, new Join(), new Death(), new Leave());
	}

	public void registerPerms() {
		PluginManager m = getServer().getPluginManager();
		// add permissions here
		m.addPermission(admin);

	}

	// add permissions here again
	public Permission admin = new Permission("autopvp.admin");

	// config create
	public static void eventRegister(org.bukkit.plugin.Plugin plugin,
			Listener... listeners) {
		for (Listener listener : listeners) {
			Bukkit.getServer().getPluginManager()
					.registerEvents(listener, plugin);
		}
	}

	private void createConfig() {
		try {
			if (!getDataFolder().exists()) {
				getDataFolder().mkdirs();
			}
			File file = new File(getDataFolder(), "config.yml");
			if (!file.exists()) {
				getLogger().info("AP: Config.yml not found, creating!");
				getConfig().addDefault("server-info.tpworld", "");
				getConfig().addDefault("players", "");
				getConfig().options().copyDefaults(true);
				saveDefaultConfig();
			} else {
				getLogger().info("AP: Config.yml found, loading!");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		AutoPvp.onPvpCommand(sender, cmd, label, args);
				return true;
	}
	
	public static Plugin getPlugin() {
		return plugin;
	}
}