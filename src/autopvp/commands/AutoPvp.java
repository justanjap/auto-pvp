package autopvp.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
//import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import autopvp.Main.Main;
import autopvp.Main.Util;

public class AutoPvp /* implements CommandExecutor */{

	public static boolean onPvpCommand(CommandSender sender, Command cmd,
			String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("autopvp")
				|| cmd.getName().equalsIgnoreCase("ap")) {
			if (!(sender instanceof Player)) {
				sender.sendMessage("You must be a player");
				return true;
			}
			Player p = (Player) sender;
			if (args.length == 0) {
				Boolean state = Util.toggle(p, "pvp-play");
				if (state == true) {
					p.sendMessage(ChatColor.GREEN + "Auto-PVP is now enabled!");
				} else {
					p.sendMessage(ChatColor.RED + "Auto-PVP is now disabled!");
				}
				return true;
			}
			switch (args[0].toLowerCase()) {
			case "start":
				if (!p.hasPermission("autopvp.admin")) {
					p.sendMessage(ChatColor.RED + "You don't have permission");
					return true;
				}
				if (Util.gameOn.get("game") == false) {
					int intPlayers = 0;
					for (Player players : Bukkit.getOnlinePlayers()){
						if (Main.getPlugin().getConfig().getBoolean("players." + players.getUniqueId() + ".toggles.pvp-play")){
							intPlayers++;
						}
						if (intPlayers >= 2){
							Util.gameStart();
							return true;
						}
					}
					p.sendMessage(ChatColor.RED + "Not enough players!");
					return true;
				}
				p.sendMessage(ChatColor.RED + "A game is in progress right now");
				p.sendMessage(ChatColor.RED
						+ "Do /autopvp end if you want to start a new game");
				return true;
			case "end":
				if (!p.hasPermission("autopvp.admin")) {
					p.sendMessage(ChatColor.RED + "You don't have permission");
					return true;
				}
				if (Util.gameOn.get("game") == true) {
					Util.gameOn.put("game", false);
					for (final Player players : Bukkit.getOnlinePlayers()) {
						if (Main.getPlugin()
								.getConfig()
								.getBoolean(
										"players." + p.getUniqueId()
												+ ".toggles.pvp-play") == true) {
							Util.gameOver(players);
						}
					}
				}
				p.sendMessage(ChatColor.RED + "No current games running!");
				return true;
			case "leave":
				if (Util.inGame.get(p) == true) {
					p.sendMessage(ChatColor.RED + "You left the fight.");
					Util.gameOver(p);
					return true;
				}
				p.sendMessage(ChatColor.RED + "You are not in a pvp fight");
				return true;
			case "help":
				help(p);
				return true;
			default:
				help(p);
				return true;
			}
		}
		return true;
	}
	private static void help(Player p) {
		p.sendMessage(ChatColor.GREEN + "/ap " + ChatColor.BLUE
				+ "Toggles if you want to be in the AutoPvp selection");
		p.sendMessage(ChatColor.GREEN + "/ap leave " + ChatColor.BLUE
				+ "Makes you leave the fight");
		if (p.hasPermission("autopvp.admin")) {
			p.sendMessage(ChatColor.GREEN + "/ap end " + ChatColor.BLUE
					+ "Makes you end the current fight");
			p.sendMessage(ChatColor.GREEN + "/ap start " + ChatColor.BLUE
					+ "Makes you start a fight");
		}
	}
}